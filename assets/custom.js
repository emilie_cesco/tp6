let prefetch
let terminus;
let saveColor;
let saveColor2;
let saveName;
let calc;
let direction = 0
let prevTime;
let nextTime
let timeString;
let temp = Date.now();
let etaBtnBus = false;
let url = "http://data.metromobilite.fr/api/routers/default/index/routes";



window.onload = function () {
  $('#listeHoraires').hide()
  $('.terminus').hide()
  $('#mapid').hide()
}


$('#btn-bus').one('click', function () {
  $('#listeHoraires').hide()
  $('.divHoraires').text('')
  $('.results').text('')
  $('.terminus').hide()
})

$('.scolaire').click(function () {
  lignesdetransports("BUS", "SCOL", 'elscol', $('.scolaire'))
});

$('.proximo').click(function () {
  lignesdetransports("BUS", "PROXIMO", 'elproximo', $('.proximo'))
});



$('.chrono').click(function () {
  lignesdetransports("BUS", "CHRONO", 'elchrono', $('.chrono'))
});

$('.flexo').click('click', function () {
  lignesdetransports("BUS", "FLEXO", 'elflexo', $('.flexo'))
});

$('.c38').one('click', function () {
  lignesdetransports("BUS", "C38", 'elc38', $('.c38'))
});


$('#btn-tram').click(function () {
  lignesdetransports("TRAM", "TRAM", 'eltram', $('#c-tab1'))
  $('.terminus').hide()
});




$('#btn-rail').click(function () {
  lignesdetransports("RAIL", "SNC", 'elrail', $('#c-tab2'))
  $('.terminus').hide()
});


let lignesdetransports = (mode, type, clas, container) => {
  $('.divHoraires').text('')
  $('#listeHoraires').hide()
  $('.results').text('')


  fetch(url)
    .then((response) => response.json())
    .then((json) => {
      $('.results').on('click', '.circle', function (e) {
        $('#mapid').show()
        prefetch = e.target.id;
        saveColor = $(this).data('color')
        saveName = $(this).data('name')
        saveColor2 = $(this).data('color2')

        fetch(`http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${prefetch}`)
        .then((response) => response.json())
        .then((json) => {
      
          console.log(json)
          
      
          var myStyle = {
            "color": `#${saveColor}`
         
        };
        
        L.geoJSON(json, {
            style: myStyle
        }).addTo(mymap);
      
        })

        horaires(e.target.id, temp)
        $('#plan').remove()
        jQuery('<span/>',
          {
            id: 'plan',
            "class": clas + ' circle',
            "style": `background-color: #${saveColor}; color: #${saveColor2}`
          })
          .text(saveName).appendTo('.p-margin');
        $('.terminus').text(terminus)
        $('.terminus').show()
      })

      for (i = 0; i < json.length; i++) {
        if (json[i].mode == mode && json[i].type == type) {

          jQuery('<a/>',
            {
              id: json[i].id,
              "href": "#listeHoraires",
              "class": clas + ' circle',
              "data-color": `${json[i].color}`,
              "data-name": `${json[i].shortName}`,
              "data-color2": `${json[i].textColor}`,
              "style": `background-color: #${json[i].color}; color: #${json[i].textColor}`,

            })
            .text(json[i].shortName).appendTo(container.children('.results'));



        }

      }
      $('.terminus').text(terminus)


    })
}



$('#direction').click(function () {
  if (direction == 0) {
    direction = 1
  } else { direction = 0 }

  horaires(prefetch, temp)
})
$('#hprecedant').click(function (e) {
  horaires(prefetch, prevTime)
})

$('#hsuivant').click(function (e) {
  horaires(prefetch, nextTime)
})


let horaires = (ligne, temp) => {
  fetch(`https://data.metromobilite.fr/api/ficheHoraires/json?route=${ligne}&time=${temp}`)
    .then((response) => response.json())
    .then((json) => {

      $('.divHoraires').text('')
      $('#listeHoraires').show()
      prevTime = json[direction].prevTime
      nextTime = json[direction].nextTime
      calc = json[direction].arrets.length
      terminus = json[direction].arrets[calc - 1].stopName
      $('.terminus').text(terminus)

      console.log(json[direction].arrets[0].lat)
      for (i = 0; i < json[direction].arrets.length; i++) {
         L.marker([json[direction].arrets[i].lat,json[direction].arrets[i].lon]).addTo(mymap)
        // permet de convertir en heure 
        let date = new Date(0);
        if (json[direction].arrets[i].trips[0] == '|') {
          date.setSeconds(0)
          timeString = date.toISOString().substr(11, 8);
        } else {
          date.setSeconds(json[direction].arrets[i].trips[0])
          timeString = date.toISOString().substr(11, 8);;
        }

        jQuery('<div/>',
          {
            "class": 'classHoraires'
          })
          .text(json[direction].arrets[i].stopName).appendTo('.divHoraires');

        jQuery('<div/>',
          {
            "class": 'classHoraires2'
          })
          .text(timeString).appendTo('.divHoraires');
      }
    })
}


//carte 
var mymap = L.map('mapid').setView([45.1667, 5.71679], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1,
  accessToken: 'pk.eyJ1IjoiZW1pbGllMzgiLCJhIjoiY2tlbGV5MHI1MWRpZjJybnA4a2VkcmNjZCJ9.r8I8wWDUC_4PwSMn5cSjGA'
}).addTo(mymap);

